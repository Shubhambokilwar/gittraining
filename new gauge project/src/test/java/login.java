package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import com.thoughtworks.gauge.*;

public class login {

        @Step("Navigate to <baseUrl>")
        public void navigatetourl(String baseUrl){
                System.setProperty("webdriver.chrome.driver", "src/libs/chromedriver");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.get(baseUrl);

        }

        @Step("Enter Username <userName>")
        public void enterUserName(String userName) {
                driver.findElement(By.xpath("//input[@placeholder='Username']")).sendKeys(userName);

        }

        @Step("Enter Password <password>")
        public void enterPassword(String password) {
                driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys(password);

        }

        @Step("Click Login Button")
        public void clickLoginButton() {
                driver.findElement(By.xpath("//span[text()='Login']")).click();
                driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
        }

}

