import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import com.thoughtworks.gauge.*;

public class loginpage {

    static WebDriver driver;

    @Step("Navigate to <baseUrl>")
    public void navigatetourl(String baseUrl) {
        System.setProperty("webdriver.chrome.driver", "src/libs/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);

    }

    @Step("Enter Username <userName>")
    public void enterUserName(String userName) {
        driver.findElement(By.id("login1")).sendKeys(userName);

    }

    @Step("Enter Password <password>")
    public void enterPassword(String password) {
        driver.findElement(By.id("login2")).sendKeys(password);
    }

    @Step("Click Login Button")
    public void clickLoginButton() {
        driver.findElement(By.id("login5")).click();

    }

    public void close() {
        driver.close();
    }
}